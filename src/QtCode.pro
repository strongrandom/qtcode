#-------------------------------------------------
#
# Project created by QtCreator 2011-10-15T19:24:40
#
#-------------------------------------------------

QT       += core gui

TARGET = QtCode
TEMPLATE = app
RC_FILE = qtcode.rc
ICON = qtcode.icns

DEFINES += HAVE_CONFIG_H

SOURCES += main.cpp\
        mainwindow.cpp \
    split.c \
    rscode.c \
    qrspec.c \
    qrinput.c \
    qrencode.c \
    mask.c \
    bitstream.c \
    mqrspec.c \
    mmask.c

HEADERS  += mainwindow.h \
    bitstream.h \
    split.h \
    rscode.h \
    qrspec.h \
    qrinput.h \
    qrencode_inner.h \
    qrencode.h \
    mask.h \
    config.h \
    mqrspec.h \
    mmask.h

FORMS    += mainwindow.ui





