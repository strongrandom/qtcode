qtcode
======

Cross platform QR code GUI. Heavily based off qrencode (does the real work).

Copyright (C) 2015 Brian Wood.
Copyright (C) 2006-2012 Kentaro Fukuchi.

License: LGPL


Introduction
------------

This is a simple GUI frontend for qrencode. I often need a QR code, but don't want to render an image file. For example, quickly transferring information to a mobile phone, or between computers using a USB 2-D imager (Honeywell 3310g or Symbol/Motorola DS6707). If you have one of these devices, it's also useful because you can scan passwords off the screen into VNC sessions or other apps that try to disable clipboard paste. 

QtCode will watch the clipboard and automatically render a QR code from its contents. You can interactively edit the barcode contents. Click "pause" to disable automatic clipboard updates.

MAC OS X: Qt does not support automatic clipboard updates. QtCode will only update when it regains focus. Click on the QtCode window or select "Edit > Manual Refresh".

In addition to clipboard and manual edits, you can pipe data into QtCode from stdin. For example:

        openssl rand -base64 8 | ./QtCode

Finally, you can save the image as various bitmap formats, and you can copy it to the clipboard.


Notes
-----

For formats supported by Zebra Crossing, see: http://code.google.com/p/zxing/wiki/BarcodeContents
See also: http://zxing.appspot.com/generator/

Google Authenticator Format: http://code.google.com/p/google-authenticator/source/browse/mobile/android/src/com/google/android/apps/authenticator/AuthenticatorActivity.java



Build/Install
------------

Requires Qt 4 (tested with 4.8.6, has not been updated for Qt 5).

        qmake ; make ; make install

        or
        
        qmake-qt4 ; make ; make install
        
        Alternatively, you can install Qt Creator and build 
        using it.


For linux, install your distributions development packages for Qt and build. Works with Qt 4.8, but should build with older versions of Qt 4.

Fedora 22: dnf install qt-devel

Note that this includes a mostly complete copy of qrencode. It should link to it instead. However, at one time, cross-platform builds were very important to me... and I wanted to make it as simple as possible to build on Windows/Mac OS X.
