/*
  QtCode - Qt GUI frontend for qrencode

  Copyright (C) 2015 Brian Wood
  Copyright (C) 2006-2012 Kentaro Fukuchi <kentaro@fukuchi.org>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QClipboard>
#include <QImage>
#include <QString>
#include <stdio.h>

#include "qrencode.h"

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *m_ui;
    QClipboard *m_clipboard;
    QImage *m_qImage;

private slots:
    void clipboardChanged();
    void updateQRCode(QString);

    void clearQRCode();

    void copyImageToClipboard();
    void saveAs();
    void manualRefresh();
};

#endif // MAINWINDOW_H
