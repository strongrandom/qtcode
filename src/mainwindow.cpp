/*
  QtCode - Qt GUI frontend for qrencode

  Copyright (C) 2015 Brian Wood
  Copyright (C) 2006-2012 Kentaro Fukuchi <kentaro@fukuchi.org>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA


  For formats supported by Zebra Crossing, see: http://code.google.com/p/zxing/wiki/BarcodeContents
  See also: http://zxing.appspot.com/generator/

  Google Authenticator Format: http://code.google.com/p/google-authenticator/source/browse/mobile/android/src/com/google/android/apps/authenticator/AuthenticatorActivity.java

*/

#include <QFileDialog>
#include <QTextStream>
#include <unistd.h>

#include "mainwindow.h"
#include "ui_mainwindow.h"

// QR Code Parameters
static int casesensitive = 1;
static int version = 0;
static int margin = 4;
static int scale = 4;
static QRecLevel level = QR_ECLEVEL_L;
static QRencodeMode hint = QR_MODE_8;

const QString qtCodeVersion = "0.91";


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_ui(new Ui::MainWindow)
{
    m_qImage = NULL;

    m_ui->setupUi(this);
    m_clipboard = QApplication::clipboard();

    QObject::connect(m_clipboard, SIGNAL(dataChanged()),
                     this, SLOT(clipboardChanged()));

    QObject::connect(m_ui->lineEdit, SIGNAL(textChanged(QString)),
                     this, SLOT(updateQRCode(QString)));

    QObject::connect(m_ui->actionCopy_Image_to_Clipboard, SIGNAL(triggered()),
                     this, SLOT(copyImageToClipboard()));

    QObject::connect(m_ui->action_Save_As, SIGNAL(triggered()),
                     this, SLOT(saveAs()));

    QObject::connect(m_ui->action_Quit_2, SIGNAL(triggered()),
                     this, SLOT(close()));

    QObject::connect(m_ui->actionManual_Refresh, SIGNAL(triggered()),
                     this, SLOT(manualRefresh()));

    QObject::connect(m_ui->actionClear, SIGNAL(triggered()),
                     this, SLOT(clearQRCode()));


#ifdef Q_WS_MAC
    // Warn the user about http://doc.qt.nokia.com/latest/qclipboard.html#dataChanged
    setWindowTitle(tr("QtCode %1 - Click To Refresh Clipboard").arg(qtCodeVersion));
#else
    setWindowTitle(tr("QtCode %1").arg(qtCodeVersion));
#endif

#ifdef QT_NO_DEBUG
    // Doesn't seem to work if started by gdb. Qt Creator will set -DQT_NO_DEBUG for release builds.

    // http://stackoverflow.com/questions/1312922/detect-if-stdin-is-a-terminal-or-pipe-in-c-c-qt
    if (isatty(fileno(stdin))) {
        // stdin is a terminal
        clipboardChanged();
    } else {
        // stdin is a file or a pipe
        QTextStream qtin(stdin);
        QString qs(qtin.readLine());

        if (qs.length() > 0)
        {
            m_ui->lineEdit->setText(qs);
            m_ui->statusBar->showMessage(tr("Input from stdin used."));
        }
    }
#else
    clipboardChanged();
#endif
}

void MainWindow::clipboardChanged()
{
    if (!m_ui->checkBox_Pause->isChecked())
    {
        // Ignore images
        if (!m_clipboard->image().isNull()) return;
        if (!m_clipboard->pixmap().isNull()) return;

        QString text = m_clipboard->text();
        m_ui->lineEdit->setText(text);
    }
}

void MainWindow::clearQRCode()
{
    QImage image = QImage(64, 64, QImage::Format_Indexed8);
    image.setColor(0, qRgb(255,255,255));
    image.fill(0);
    m_ui->label->setPixmap(QPixmap::fromImage(image));

    m_ui->lineEdit->blockSignals(true);
    m_ui->lineEdit->setText("");
    m_ui->lineEdit->blockSignals(false);

    if (m_qImage != NULL) delete(m_qImage);
    m_qImage = NULL;
}

void MainWindow::updateQRCode(QString text)
{
    if (text.length() == 0) {
        m_ui->statusBar->showMessage(tr("Empty string."));
        clearQRCode();
        return;
    }

    QRcode *qrcode = QRcode_encodeString(text.toLocal8Bit(), version, level, hint, casesensitive);

    if (qrcode == NULL) {
        clearQRCode();
        m_ui->statusBar->showMessage(tr("Failed to encode the input data (Too large? %1 bytes).").arg(text.length()));
        return;
    }

    unsigned char *p;
    int x, y, bit;
    int realwidth;

    realwidth = qrcode->width + margin * 2;

    QImage image = QImage(realwidth, realwidth, QImage::Format_Indexed8);
    QRgb value;

    value = qRgb(255,255,255); // 0xff7aa327
    image.setColor(0, value);

    value = qRgb(0,0,0); // 0xffedba31
    image.setColor(1, value);
    image.setColor(2, value);

    image.fill(0);

    p = qrcode->data;
    for(y=0; y<qrcode->width; y++) {
        bit = 7 - (margin % 8);
        for(x=0; x<qrcode->width; x++) {

            if ((*p & 1) << bit)
                image.setPixel(x+margin, y+margin, 1);
            else
                image.setPixel(x+margin, y+margin, 0);

            bit--;
            if(bit < 0) {
                bit = 7;
            }

            p++;
        }
    }

    if (m_qImage != NULL) delete(m_qImage);
    m_qImage = new QImage(image.scaledToWidth(realwidth * scale));

    m_ui->label->setPixmap(QPixmap::fromImage(*m_qImage));
    m_ui->statusBar->showMessage(tr("%1 bytes. %2 x %2 pixels.").arg(text.length()).arg(realwidth-margin));

    // There is probably a better way to do this, but this effectively causes the window to shrink, rather than perpetually grow.
    this->resize(m_qImage->size());
}

void MainWindow::copyImageToClipboard()
{
    if (m_qImage == NULL)
    {
        m_ui->statusBar->showMessage(tr("No image to copy."));
        return;
    }

    m_clipboard->setImage(*m_qImage);
    m_ui->statusBar->showMessage(tr("Copied to clipboard."));
}

void MainWindow::saveAs()
{
    if (m_qImage == NULL)
    {
        m_ui->statusBar->showMessage(tr("No image to save."));
        return;
    }

    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Save Image"), "",
                                                    tr("Portable Network Graphics - PNG (*.png);;Windows Bitmap - BMP (*.bmp);;Joint Photographic Experts Group - JPG (*.jpg);;Joint Photographic Experts Group - JPEG (*.jpeg);;Portable Pixmap - PPM (*.ppm);;Tagged Image File Format - TIFF (*.tiff);;X11 Bitmap - XBM (*.xbm);;X11 Bitmap - XPM (*.xpm)"));
    if (fileName.length() > 0)
    {
        if (m_qImage->save(fileName))
            m_ui->statusBar->showMessage(tr("Saved %1").arg(fileName));
        else
            m_ui->statusBar->showMessage(tr("Saved failed."));
    }
}

void MainWindow::manualRefresh()
{
    QString text = m_clipboard->text();
    m_ui->lineEdit->setText(text);
}

MainWindow::~MainWindow()
{
    if (m_qImage != NULL) delete(m_qImage);
    delete m_ui;
}
